## 36Kr 手机app开发

本项目基于mui二次开发，定位是资讯类的app

## 页面结构

- guide.html 欢迎页，第一次启动app后加载。
- index.html app启动页面，主要用于初始化（PS：并不在页面中显示实际的内容）。
- index-menu.html 侧边栏菜单
- info.html 关于信息（app制作人员，版本号）
- list.html 首屏（展示主要的内容）
- template.html 模板页（没有实际内容，仅供参考） 